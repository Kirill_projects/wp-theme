function CustomMarker(latlng,  map, template) {
    this.latlng_ = latlng;
    this.template = template;
    this.setMap(map);
  }

  CustomMarker.prototype = new google.maps.OverlayView();
  CustomMarker.prototype.draw = function() {
     
      var me = this;
      var div = this.div_;
      if (!div) {
        div = this.div_ = document.createElement('DIV');
        div.innerHTML = this.template;
        div.style.border = "none";
        div.style.height = "300px";
        div.style.width = "300px";
        div.style.position = "absolute";
        div.style.paddingLeft = "0px";
        div.style.cursor = 'pointer';
        //you could/should do most of the above via styling the class added below
        div.classList.add('o-marker');

        google.maps.event.addDomListener(div, "click", function(event) {
          google.maps.event.trigger(me, "click");
        });

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
      }

      var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
      if (point && window.innerWidth > 767) {
        div.style.left = point.x + 'px';
        div.style.top = point.y + 'px';
      }else {
          div.style.left = '0px';
          div.style.right = '0px';
          div.style.top = '10px';
          div.style.margin = 'auto';
      }
  };

  CustomMarker.prototype.remove = function() {
    if (this.div_) {
      this.div_.parentNode.removeChild(this.div_);
      this.div_ = null;
    }
  };

  CustomMarker.prototype.getPosition = function() {
   return this.latlng_;
  };