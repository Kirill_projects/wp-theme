<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

$myTheme = wp_get_theme();
define('TTDOMAIN', $myTheme->get( 'TextDomain' ) );
define('TVER', $myTheme->get( 'Version' ) );


load_theme_textdomain( TTDOMAIN, get_template_directory() .'/languages' );
if(function_exists('pll_register_string')){
    pll_register_string(TTDOMAIN, 'SALON');
    pll_register_string(TTDOMAIN, 'OUTLET');
    pll_register_string(TTDOMAIN, 'STORE');
    pll_register_string(TTDOMAIN, 'PHONES');    
    pll_register_string(TTDOMAIN, 'HOURS');    
    pll_register_string(TTDOMAIN, 'MAP_TITLE');    
    pll_register_string(TTDOMAIN, 'RESPOND_BUTTON');   
    pll_register_string(TTDOMAIN, 'REQUISITES_TITLE');    
    pll_register_string(TTDOMAIN, 'JURIDICAL_TITLE');   
    pll_register_string(TTDOMAIN, 'CONTACTS_TITLE');   
    pll_register_string(TTDOMAIN, 'INSTA_TITLE');   
    pll_register_string(TTDOMAIN, 'FB_TITLE');   
    pll_register_string(TTDOMAIN, 'DOWNLOAD_BTN_TITLE');   
    pll_register_string(TTDOMAIN, 'FOOTER_TEXT');   


    pll_register_string('bwnuts', 'Variety');
    pll_register_string('bwnuts', 'Data');
    pll_register_string('bwnuts', 'Data Header1');
    pll_register_string('bwnuts', 'Data Header2');
    pll_register_string('bwnuts', 'Data Header3');
    pll_register_string('bwnuts', 'Data Header4');    
}


/**
 * Theme Includes
 */

function svg_upload ($svg_mime) {
    $svg_mime['svg'] = 'image/svg+xml';
    $svg_mime['ogv'] = 'video/ogg';
    return $svg_mime;
}
add_filter( 'upload_mimes', 'svg_upload' );

add_image_size( 'square-thumb',200, 200, true );
add_image_size( 'news-thumb',380, 280, true );
add_image_size( 'news-thumb-sml',318, 179, true ); 
add_image_size( 'news-big',800, 800, false); 
add_image_size( 'hero-tmb',500, 500, false); 
add_image_size( 'prod-thumb',500, 500, true); 

 
add_filter( 'image_size_names_choose', 'cti_custom_sizes' );
function cti_custom_sizes( $sizes ) {
  return array_merge( $sizes, array(
    'news-thumb' => 'Thumbnail',
    'news-thumb-sml' => 'Small Thumbnail',
    'news-big' => 'Full Width',
    'prod-thumb' => 'Product Thumbnail'
  ) );
}


 
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

require_once get_template_directory() .'/inc/init.php';
if (defined('FW')){
add_action('fw_init', function(){
    remove_filter('the_content', array(fw_ext('page-builder'), '_theme_filter_prevent_autop'), 1);
    

});

remove_filter( 'the_content', 'wpautop');
remove_filter( 'the_excerpt','wpautop');
//  remove_filter( 'the_content', array( fw_ext( 'page-builder' ) , '_theme_filter_prevent_autop' ), 1 ); 
}




function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => false,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => true,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}


// Hook for adding admin menus
add_action('admin_menu', 'mt_add_pages');
// action function for above hook
function mt_add_pages() {
 

    // Add a new top-level menu (ill-advised):
    add_menu_page(__('Настройки темы','menu-test'), __('Настройки темы','menu-test'), 'manage_options', 'themes.php?page=fw-settings', '', '', 2 ); 
}

/**
 * TGM Plugin Activation
 */
{
  require_once dirname( __FILE__ ) . '/TGM-Plugin-Activation/class-tgm-plugin-activation.php';

  /** @internal */
  function _action_theme_register_required_plugins() {
    tgmpa( array(
      array(
        'name'      => 'Unyson',
        'slug'      => 'unyson',
        'required'  => true,
      ),
      array(
        'name'      => 'Polylang',
        'slug'      => 'polylang',
        'required'  => false,
      ),
      array(
        'name'      => 'WooCommerce',
        'slug'      => 'woocommerce',
        'required'  => true,
      ),
      array(
        'name'      => 'WooCommerce Catalog',
        'slug'      => 'woocommerce-catalog',
        'required'  => false,
      ),
      
    ) );

  }
  add_action( 'tgmpa_register', '_action_theme_register_required_plugins' );
}
// declare WC support
function artizana_wc_support() {
  add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'artizana_wc_support' );


// wp_enqueue_script('main', get_template_directory_uri(). '/js/main.js', array('jquery'),ILS_A_VERSION, true);


function get_products_list() {
  
    $params = array('post_type' => 'product','posts_per_page' => -1); 
    $wc_query = new WP_Query($params); 
    $elements = array('select'=>__('Выберите товар', 'fw') );
    $products = get_posts( $params );
    foreach ($products as $p) {
        $elements[$p->post_title]=array(
        'text'=>__($p->post_title, 'fw'),
        //'attr' => array('data-href' => get_permalink($p->ID),'data-id'=> $p->ID),
        );
    }
    return $elements;
}


function get_cat_list() {
    $cat_list = get_categories();
    //print_r($cat_list);die();
    $params = array('post_type' => 'product','posts_per_page' => -1); 
    $wc_query = new WP_Query($params); 
    $elements = array('select'=>__('Выберите товар', 'fw') );
    $products = get_posts( $params );
    foreach ($cat_list as $c) {
        $elements[$c->cat_name]=array(
        'text'=>__($c->cat_name, 'fw'),
        'attr' => array('data-href' => get_permalink($p->ID),'data-id'=> $p->ID),
        );
    }
    return $elements;
}

add_action( 'admin_enqueue_scripts', 'admin_scripts' );

function admin_scripts() {
    wp_register_script('admin-scripts', get_template_directory_uri() . '/js/admin.js' );        
    wp_enqueue_script('admin-scripts', array( 'fw-events', 'jquery' ));
}

if ( ! function_exists( "mg_maybe_include" ) ) {
    function mg_maybe_include( $conditions ) {
        // Include in back-end only
        if ( ! defined( "WP_ADMIN" ) || ! WP_ADMIN ) {
            return false;
        }

        // Always include for ajax
        if ( defined( "DOING_AJAX" ) && DOING_AJAX ) {
            return true;
        }

        if ( isset( $_GET["post"] ) ) {
            $post_id = $_GET["post"];
        }
        elseif ( isset( $_POST["post_ID"] ) ) {
            $post_id = $_POST["post_ID"];
        }
        else {
            $post_id = false;
        }

        $post_id = (int) $post_id;
        $post    = get_post( $post_id );



        foreach ( $conditions as $cond => $v ) {
            // Catch non-arrays too
            if ( ! is_array( $v ) ) {
                $v = array( $v );
            }

            switch ( $cond ) {
                case "id":
                    if ( in_array( $post_id, $v ) ) {
                        return true;
                    }
                break;
                case "parent":
                    $post_parent = $post->post_parent;
                    if ( in_array( $post_parent, $v ) ) {
                        return true;
                    }
                break;
                case "slug":
                    $post_slug = $post->post_name;
                    if ( in_array( $post_slug, $v ) ) {
                        return true;
                    }
                break;
                case "template":
                    $template = get_post_meta( $post_id, "_wp_page_template", true );
                    if ( in_array( $template, $v ) ) {
                        return true;
                    }
                break;
                case "not_template":
                    $template = get_post_meta( $post_id, "_wp_page_template", true );
                    if ( in_array( $template, $v ) ) {
                        return false;
                    } else {
                        return true;
                    }
                break;
            }
        }

        // If no condition matched
        return false;
    }
}


/*removes paginaton and displays all  products in one page*/
add_action( 'woocommerce_product_query', 'all_products_query' );

function all_products_query( $q ){
    $q->set( 'posts_per_page', -1 );
}



add_filter( 'woocommerce_product_tabs', 'woo_custom_description_tab', 98 );
function woo_custom_description_tab( $tabs ) {

  $tabs['description']['callback'] = 'woo_custom_description_tab_content';  // Custom description callback

  return $tabs;
}

function woo_custom_description_tab_content($width) {
    global $product;

    if($width == 'sm') {
        echo '<div class="c-product__info c-product__info--sm">';
        echo woocommerce_breadcrumb(array('delimiter'=>'<span>/</span>'));
        echo '<span class="c-product__status">'.$product->get_stock_status().'</span>';
        echo '<h3 class="c-product__header">'.  $product->get_name().'</h3>';
        echo '</div>';
    
    }else {

        echo woocommerce_breadcrumb(array('delimiter'=>'<span class="delimiter">/</span>'));
        echo '<span class="c-product__status">'.$product->get_stock_status().'</span>';
        echo '<h3 class="c-product__header c-product__header--lg js-title">'.  $product->get_name().'</h3>';

        echo woocommerce_template_single_price();
        echo '<div class="c-product__desc-wrap">'. $product->get_description() .'</div>';
        echo '<span class="o-primary-btn o-primary-btn--reservation js-popup" data-href=".c-reserve">'. pll__("RESERVATION_BUTTON") . '</span>';
    }
}

