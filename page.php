<?php
/**
 * The Template for displaying all single posts
 */

get_header(); 
		if ( have_posts() ) :
		while ( have_posts() ) : the_post();	
?>
 <section class="container posts">
            <h1 class="big-header__green text-uppercase big-header__center">
            
                <img src="<?php echo get_template_directory_uri(); ?>/img/header-line-left.png" alt="">
                <?php the_title(); ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/header-line-right.png" alt="">
            </h1>
            <?php the_content(); ?>
        </section>
		    
     				 
<?php
		endwhile;
			endif;
get_footer();
