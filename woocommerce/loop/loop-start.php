<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
?>
<div class="l-container l-products">
	<div class="l-products-list" style="overflow:hidden;height:1600px;">
		<?php woocommerce_breadcrumb(array('delimiter'=>'<span class="delimiter">/</span>')); ?>
		<div class="u-grid-preloader"></div>
		<div class="products-grid w-clearfix">
