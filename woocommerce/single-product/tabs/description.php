<?php
/**
 * Description tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Description', 'woocommerce' ) ) );

?>

<!-- page template in functions.php  woo_custom_description_tab_content-->

<?php global $product; echo $product->get_stock_status(); ?>

<?php if ( $heading ) : ?>
  <!-- <h2><?php echo $heading; ?></h2> -->
<?php endif; ?>
<?php the_title( '<h3 class="c-product__header js-title">', '</h3>' ); ?>
<?php woocommerce_template_single_price(); ?>
<div class="c-product__desc-wrap">
	<?php the_content(); ?>
</div>
<span class="o-primary-btn o-primary-btn--reservation js-popup" data-href=".c-reserve">Reserve for fitting</span>
