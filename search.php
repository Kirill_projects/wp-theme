<?php
/**
 * Template Name: News Grid Page
 */
get_header();  

  $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

  $query_args = array(
      'post_type' => 'post',
      'posts_per_page' => 12,
      'paged' => $paged,
      'page' => $paged,
	   's' => get_query_var('s')
    );

  $the_query = new WP_Query( $query_args ); ?>
<section class="sctn-body">
		<div class="content-wrapper">
			<div class="h2-wrapper">
            <h1 class="h1">Search Results for  &quot;<?=get_query_var('s')?>&quot;</h1>
           <?php  
			     

    		$subtitle = fw_get_db_post_option(get_the_ID(), 'subtitle', ''); 
			if(!empty($subtitle)){
			?><div class="subtitle">
					<?=$subtitle?>
				</div>
            <?php } ?>    
			</div>
			<section class="news-container gs-clearfix">
				
				 <?php if ( $the_query->have_posts() ) : ?>

                    <!-- the loop -->
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <article class="col-3 news" itemscope itemtype="http://schema.org/NewsArticle">
					<div class="gs-hidden" itemprop="author" itemscope itemtype="https://schema.org/Organization">
						<span itemprop="name"><?=$_SERVER['HTTP_HOST']?></span>
					</div>
					<div class="news-item-container">
						<figure class="news-img-wrapper">
							<a href="<?=get_permalink()?>">
								<img class="news-img" src="<?php the_post_thumbnail_url('news-thumb')?>" itemprop="image">
								<meta itemprop="url" content="<?php the_post_thumbnail_url('news-thumb')?>">
							</a>
						</figure>
						<div class="news-item-content">
							<h3 class="h3-green" itemprop="headline">
								<a href="<?=get_permalink()?>" itemprop="url"><?php the_title(); ?></a>
							</h3>
							<div class="news-txt-wrapper">
								<div class="txt-reg" itemprop="description">
									<?php the_excerpt(); ?>
								</div>
							</div>
						</div>
						
						<div class="news-data-container gs-clearfix">
							<div class="news-date-wrapper">
								<div class="txt-small">
									<date itemprop="datePublished"><?=get_the_date()?></date>
								</div>
							</div>
						</div>
					</div>
				</article>
                    <?php endwhile; ?>
                    <!-- end of the loop -->
                <br clear="all">
                    <!-- pagination here -->
                    <?php
                      if (function_exists('custom_pagination')) {
                        custom_pagination($the_query->max_num_pages,"",$paged);
                      }
                    ?>
                
                  <?php wp_reset_postdata(); ?>
                
                  <?php else:  ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                  <?php endif; ?>				 
			</div>
		</div>
	</section>
  
<?php get_footer();