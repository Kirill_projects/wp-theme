 <div class="c-hero">
    <div class="c-hero__media">
        <img class="c-hero__img" srcset="<?php echo get_template_directory_uri(); ?>/images/hero.png  1920w,<?php echo get_template_directory_uri(); ?>/images/hero.png  960w, <?php echo get_template_directory_uri(); ?>/images/hero-m.png  768w" src="<?php echo get_template_directory_uri(); ?>/images/hero.png" alt="artizana fashion">
    </div>
    <div class="o-hero-title o-hero-title--desktop">
        <h1 class="o-hero-title__main">Верхняя одежда премиум-класса от  мировых брендов</h1>
        <div class="o-hero-title__subtitle">Наша компания представляет верхнюю одежду от ведущих мировых производителей, в которой совмещены строгость делового стиля и последние модные тенденции.</div>
    </div>
</div>
<div class="o-hero-title o-hero-title--mobile">
    <h1 class="o-hero-title__main">Верхняя одежда премиум-класса от  мировых брендов</h1>
    <div class="o-hero-title__subtitle o-hero-title__subtitle--mobile">Наша компания представляет верхнюю одежду от ведущих мировых производителей, в которой совмещены строгость делового стиля и последние модные тенденции.</div>
</div>