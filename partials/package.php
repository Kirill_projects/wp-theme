 <section class="container boxes">
             <div class="boxes-content">
            <?php 
            $p_cats = get_terms( 'pack_cat', array(
                'hide_empty' => false,
            ));
            foreach($p_cats as $pcat){
            ?>
            <div class="row pack-cat">
            <div class="h">
                <h3><?php echo $pcat->name;?></h3>
            </div>    
               <?php 
              
                $packs = new WP_Query(array(
                    'post_type' => 'pack',
                    'tax_query' => array(
                        array(
                        'taxonomy' => 'pack_cat',
                        'field' => 'id',
                        'terms' => $pcat->term_id
                         )
                      )
                    )); 
               
                if ( $packs->have_posts() ) {
                    while ( $packs->have_posts() ) {
                        $packs->the_post(); ?>
                <div class="col-md-6 pack-item">
                    <?php the_title( '<h5>', '</h5>'); ?>
                    <img style="width: 80%; height: auto; display: block; " src="<?php the_post_thumbnail_url('full')?>" alt="<?php the_title(); ?>"> 
    
                    <p><?php echo fw_get_db_post_option(get_the_ID(), 'detail');?></p>
                </div>
                <?php }; }
                wp_reset_postdata();
                ?>
                    
            </div>
            <?php
            };
            ?>
                    
                    
            </div>
</section>