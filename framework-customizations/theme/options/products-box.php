<?php
if ( !defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'demo' => array(
		'title' => __( 'Данные продукта', 'unyson' ),
		'type' => 'tab',
		'options' => array(
			'items' => array(
				'label' => 'Галерея продукта',
				'popup-title' => 'Добавить',
				'type' => 'addable-popup',
				'template' => '<img src="{{=list_image.url}}" style="width:80px; height:80px; display:block;"> {{=list_text}}',
				'popup-options' => array(
					'list_image' => array(
						'label' => 'Изображение',
						'desc' => 'Рекомендовано: 800x800 (PNG)',
						'type' => 'upload',
					),
					'list_text' => array(
						'label' => 'Alt. Текст',
						'type' => 'text'
					)

				)
			),
			'desc' => array(
				'label' => __( 'Описание', 'unyson' ),
				'type' => 'text',
			),
			'price' => array(
				'label' => __( 'Цена', 'unyson' ),
				'type' => 'text',
			),

			'bg_color' => array(
				'label' => __( 'Цвет по наведению', 'unyson' ),
				'type' => 'text',
			),
			/*'sizes_d' => array(
				'label' => __( 'СРЕДНИЕ РАЗМЕРЫ (мм): D', 'unyson' ),
				'type' => 'text',
			),
			'sizes_di' => array(
				'label' => __( 'СРЕДНИЕ РАЗМЕРЫ (мм): d', 'unyson' ),
				'type' => 'text',
			),
			'weight_core_ratio' => array(
				'label' => __( 'ВЕС ЯДРА ОТ ВЕСА ОРЕХА (%)', 'unyson' ),
				'type' => 'text',
			),
			'color' => array(
				'label' => __( 'ЦВЕТ ЯДРА ОРЕХА', 'unyson' ),
				'type' => 'text',
			)*/
		),
	),
);