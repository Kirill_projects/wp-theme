<?php
if ( !defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'demo' => array(
		'title' => __( 'Описание', 'unyson' ),
		'type' => 'tab',
		'options' => array(
			'detail' => array(
				'label' => __( 'Описание упаковки', 'unyson' ),
				'type' => 'text',
			)
		),
	),
);