<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'demo' => array(
		'title'   => __( 'Additional Settings', 'unyson' ),
		'type'    => 'tab',
		'options' => array(
			'subtitle' => array(
				'label' => __( 'Page Subtitle', 'unyson' ),
				'type'  => 'textarea',
			),
		),
	),
);
