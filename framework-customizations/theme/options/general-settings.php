<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general' => array(
		'title'   => __( 'Общие', 'unyson' ),
		'type'    => 'tab',
		'options' => array(
			'general-box' => array(
				'title'   => __( 'Общие настройки', 'unyson' ),
				'type'    => 'box',
				'options' => array(
					'sitename'    => array(
						'label' => __( 'Название сайта', 'unyson' ),
						'desc'  => __( 'Укажите название сайта', 'unyson' ),
						'type'  => 'text',
						'value' => get_bloginfo( 'name' )
					),

					'product_placeholder' => array(
						'label'         => 'Placeholder товара',
						'type'          => 'upload'
					),

					/*'gallery' => array(
						'label'         => 'Фотогаллерея',
						'popup-title'   => 'Добавить',
						'desc'          => 'Добавьте сертификат',
						'type'          => 'multi-upload'
					)*/
                    /*'header_video_webm'=> array(
                    'label' => __( 'Видео в шапке на главной(.webm)','unyson'),
                    'type' => 'upload'
                     ),
                    'header_video_mp4'=> array(
                    'label' => __( 'Видео в шапке на главной(.mp4)','unyson'),
                    'type' => 'upload'
                     ),
                    'header_video_ogv'=> array(
                    'label' => __( 'Видео в шапке на главной(.ogv)','unyson'),
                    'type' => 'upload'
                     ),*/
					/*'logo_home_head' => array(
						'label' => __( 'Логотип в шапке на главной', 'unyson' ),
						'desc'  => __( '', 'unyson' ),
						'type'  => 'upload'
					),
					'logo_head' => array(
						'label' => __( 'Логотип в шапке', 'unyson' ),
						'desc'  => __( '', 'unyson' ),
						'type'  => 'upload'
					),*/
                   /* 'logo_footer' => array(
						'label' => __( 'Логотип в нижней части', 'unyson' ),
						'desc'  => __( '', 'unyson' ),
						'type'  => 'upload'
					),*/
					
					
					
					/*'prefooter-text' => array(
						'label' => __( 'Pre-Footer Text', 'unyson' ),
						'desc'  => __( 'Ex.: WANT TO KNOW MORE ABOUT US ?', 'unyson' ),
						'type'  => 'text'
					),
					'prefooter-link' => array(
						'label' => __( 'Pre-Footer Text Button Link', 'unyson' ),
						'desc'  => __( 'Ex.: /about-us/', 'unyson' ),
						'type'  => 'text'
					),*/
					
				)
			),
			
		)
	),
	'contact' => array(
		'title'   => __( 'Контакты и Соц. Профиль', 'unyson' ),
		'type'    => 'tab',
		'options' => array(
			'contact-box' => array(
				'title'   => __( 'Детали', 'unyson' ),
				'type'    => 'box',
				'options' => array( 
							'company' => array(
								'label' => 'Название компании',
								'type'  => 'text',
							),
							'phone'         => array(
								'label' => 'Телефон',
								'type'  => 'text'
							),
							'cmail' => array(
								'label' => __( 'Контактный Email', 'unyson' ),
								'type'  => 'text'
							),

							'requisites'         => array(
								'label' => 'Реквизиты',
								'type'  => 'textarea'
							),
							'fbapp' => array(
								'label' => __( 'Facebook App ID', 'unyson' ),
								'type'  => 'text'
							),
							'fb-link' => array(
								'label' => __( 'Facebook', 'unyson' ),
								'type'  => 'text'
							),
							'insta-link' => array(
								'label' => __( 'Instagram', 'unyson' ),
								'type'  => 'text'
							),

							'map_center' => array(
			                    'label' => 'Центр карты',
			                    'type'  => 'text'	
							),

							/*'salon_pin' => array(
			                    'label' => 'Маркер салона',
			                    'type'  => 'text'	
							),

							'outlet_pin' => array(
			                    'label' => 'Маркер аутлета',
			                    'type'  => 'text'	
							),

							'store_pin' => array(
			                    'label' => 'Маркер магазина',
			                    'type'  => 'text'	
							),

							'salon_workinghours_' => array(
			                    'label' => 'Часы работы салона',
			                    'type'  => 'text'	
							),

							'outlet_workinghours_'=> array(
				                    'label' => 'Часы работы аутлета',
				                    'type'  => 'text'	
									),
							'store_workinghours_'=> array(
			                    'label' => 'Часы работы магазина',
			                    'type'  => 'text'	
								)*/
							
							/*'url'   => array(
								'label' => 'Адресс Сайта',
								'type'  => 'text',
							),*/
/*
							'map_center'   => array(
								'label' => 'Map Center',
								'type'  => 'text',
								'desc'    => '<strong>Ex.:</strong> 55.8809:-3.970999',
							), 
							'map_marker'   => array(
								'label' => 'Map Marker',
								'type'  => 'text',
								'desc'    => '<strong>Ex.:</strong> 55.8809:-3.970999',
							)
*/
							
				)
			)
		)
	)
);

$translations = pll_languages_list();

foreach($translations as $slug => $lang){
	
	$options['lang_'.$lang]=  array(
		'title'   => __( 'Перевод ('.strtoupper($lang).')', 'unyson' ),
		'type'    => 'tab',
		'options' => array(

			'places'=> array(
				'type'  => 'addable-box',
			    'value' => array(
			        array(
			            'option_1' => 'Адрес 1',
			            'option_2' => 'Адрес 2',
			        ),
			        // ...
			    ),
			    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
			    'label' => __('Адреса', '{domain}'),
			    'desc'  => __('Description', '{domain}'),
			    'box-options' => array(
			        'option_1' => array( 'type' => 'text' ),
			        'option_2' => array( 'type' => 'upload' ),
			    ),
			    'template' => '{{- option_1 }}', // box title
			    'box-controls' => array( // buttons next to (x) remove box button
			        'control-id' => '<small class="dashicons dashicons-smiley"></small>',
			    ),
			    'limit' => 0, // limit the number of boxes that can be added
			    'add-button-text' => __('Add', '{domain}'),
			    'sortable' => true,
			),
			'text-copy_'.$lang => array(
                    'label' => __( 'Копирайт', 'unyson' ),
                    'type'  => 'text'
					)
           /* 'header_image_mob_'.$lang => array(
                    'label' => __('Изображение Моб. (Вкусные орехи) 270X250', 'unyson'),
                    'type' => 'upload'
                     ),
            'header_image_'.$lang => array(
                    'label' => __('Изображение (Вкусные орехи) 670X280', 'unyson'),
                    'type' => 'upload'
                     )*/
		)
	);
	
}



// Langed Options 