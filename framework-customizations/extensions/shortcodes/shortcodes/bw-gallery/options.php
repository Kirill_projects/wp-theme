<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'         => array(
		'label' => 'Заголовок',
		'type'  => 'text',
	),
	'gallery' => array(
		'label'         => 'Фотогаллерея',
		'popup-title'   => 'Добавить',
		'desc'          => 'Добавьте фото которые хотите показать в фотогаллерее',
		'type'          => 'addable-popup',
		'template'      => '<img src="{{=list_image.url}}" style="width:80px; height:80px; display:block;">{{=list_heading}}',
		'popup-options' => array(
			'list_image' => array(
				'label' => 'Фото',
				'type'  => 'upload',
			),
			'list_heading'   => array(
				'label' =>  'Заголовок Фотографии',
				'type'  => 'text',
			)  
			
			
		)
	)
 
);
?>
