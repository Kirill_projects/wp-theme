<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'bw-gallery-');
if(!empty($atts['title'])){
?>  <section class="container-fluid post-header">            
            <h2 class="small-header text-uppercase"><?php echo $atts['title'];?></h2>
            <hr class='small-header__line top-header__line'>    
	    </section>
<?php } ?>	    
	    <section class="container mb40">
            <div id="gallery-slider" class="gslider">
                   <?php foreach ($atts['gallery'] as $step){ 

                            $icon  = !empty($step['list_image']['url'])
                                        ?wp_get_attachment_image_src($step['list_image']['attachment_id'], 'full')
										:false ;
$thumb =  wp_get_attachment_image_src($step['list_image']['attachment_id'], 'square-thumb');

if(!$icon) continue; 
    
?><div class='gallery-slider__item'><img src="<?php echo $thumb[0];?>" width="170" height="170" data-src="<?php echo $icon[0]; ?>"   data-w="<?php echo $icon[1]; ?>" data-h="<?php echo $icon[2]; ?>"  alt="<?php echo $step['list_heading']; ?>"></div><?php } ?>
           </div>
            <?php /*?><div id="open-gallery">
                <a href="#">
                    <span>ВСЯ ГАЛЕРЕЯ</span>
                    <img src="img/slider-images/gallery-open.png" alt="" >
                </a>    
            </div><?php */?>           
</section>