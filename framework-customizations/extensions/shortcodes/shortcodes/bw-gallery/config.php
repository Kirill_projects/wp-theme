<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Слайдер -  галерея', 'fw' ),
		'description' => __( 'Блок со слайдами', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);