<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'slider-' );
?>  
<section class="container nuts">
<div>

  <?php echo $atts['prod_cat'];?>
                  <?php 
    query_posts(array( 
        'post_type' => 'products',
        'showposts' => 24,
        'category_name'=>$atts['prod_cat']
    ) );  
  while (have_posts()){ the_post(); ?>
    <div class="nuts-slider__item"><a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('prod-thumb')?>" alt="<?php the_title(); ?>"></a>
                    <div><?php the_title(); ?></div>
                    </div>
                    
  <?php } wp_reset_query();?>
</div>
</section>