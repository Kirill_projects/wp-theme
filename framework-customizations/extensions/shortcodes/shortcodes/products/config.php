<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Блок с продуктами', 'fw' ),
		'description' => __( 'Блок со сеткой продуктов', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);