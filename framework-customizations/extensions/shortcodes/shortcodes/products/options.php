<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'         => array(
		'label' => 'Header',
		'desc'  => 'Section Header',
		'type'  => 'text',
	),

	'prod_cat'         => array(
		'label' => 'Категория',
		'desc'  => 'Категория продукции',
		'type'  => 'select',
		'choices' => get_cat_list(),
	),	 

	'bg_color'         => array(
		'label' => 'Цвет сетки',
		'desc'  => 'Фоновый цвет сетки',
		'type'  => 'text'
	),	 
);