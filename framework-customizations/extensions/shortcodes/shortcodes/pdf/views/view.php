<?php if ( ! defined( 'FW' )) {
	die( 'Forbidden' );
}
$id = uniqid( 'file-' );
?><div class="c-brand">
  <div class="c-brand__desc"><a href="<?php echo $atts['file']['url'];?>"><?php echo $atts['file'];?></a></div>
  <span><?php echo size_format(filesize( get_attached_file( $atts['file']['attachment_id']))); ?></span>
</div>
