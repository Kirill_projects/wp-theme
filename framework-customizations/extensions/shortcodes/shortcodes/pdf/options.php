<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'file'       => array(
		'label' => 'PDF Файл',
		'type'  => 'upload',
	)
);