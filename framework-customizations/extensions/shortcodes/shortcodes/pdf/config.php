<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'PDF блок', 'fw' ),
		'description' => __( 'PDF файл', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
		'disable_correction'=>true,
		'disable_columns_auto_wrap'=>true
	)
);
