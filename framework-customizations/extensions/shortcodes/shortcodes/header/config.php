<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Заголовок', 'fw' ),
		'description' => __( 'Заголовок - линия + текст', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);