<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
    'title'         => array(
		'label' => 'Заголовок',
		'desc'  => 'необязательно',
		'type'  => 'text',
	),
	'text'       => array(
		'label' => 'Текст',
		'desc'  => 'необязательно',
		'type'  => 'textarea',
	)
);