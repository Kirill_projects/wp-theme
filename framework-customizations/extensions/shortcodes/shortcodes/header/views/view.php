<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'header-' );
?><section class="container-fluid post-header"><?php if(!empty($atts['title'])){ ?>        
            <h2 class="small-header text-uppercase" ><?php echo $atts['title']; ?></h2>
            <hr class='small-header__line top-header__line'><?php  } ?> 
            <?php if(!empty($atts['text'])){ ?>
            <div class="row">
                <div class="col-md-12 post">
                    <div class="post-text"><?php echo nl2br($atts['text']); ?></div>
                </div            
            </div>  
            <?php  } ?> 
</section>