<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( '1col-img-' );

$img  = !empty($atts['img']['url'])
			? $atts['img']['url']
			: false;

$img2  = !empty($atts['img2']['url'])
            ? $atts['img2']['url']
            : false;

$img3  = !empty($atts['img3']['url'])
            ? $atts['img3']['url']
            : false;


?>
<section class="l-index l-season">   
    <div class="c-season">        
        <div class="c-season__left">
            
                <h3 class="c-season__title"><?php echo $atts['title']; ?></h3>
            
           
                <div class="c-season__info"><?php echo $atts['text1']; ?></div>
            
            <a href="<?php echo get_site_url().'/'.$atts['btn_url']; ?>" class="o-primary-btn o-primary-btn--nav-lookbook"><?php echo $atts['btn_label']; ?></a>
        </div>
        <div class="c-season__right">
            <div class="u-expander"></div>
            <?php if($img){ ?>
            <div class="order-1 c-season__media">
                <img src="<?php echo $img;?>" alt="<?php echo $atts['title']; ?>">
            </div>
            <?php } ?>
            <?php if($img2){ ?>
            <div class="order-2 c-season__media">
                <img src="<?php echo $img2;?>" alt="<?php echo $atts['title']; ?>">
            </div>
            <?php } ?>
            <?php if($img3){ ?>
            <div class="order-3 c-season__media">
                <img src="<?php echo $img3;?>" alt="<?php echo $atts['title']; ?>">
            </div>
            <?php } ?>
        </div>
    </div>
</section>