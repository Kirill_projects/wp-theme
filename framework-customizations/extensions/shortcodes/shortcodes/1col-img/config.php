<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Одна колонка и 3 фото', 'fw' ),
		'description' => __( 'Заголовок - линия + одна колонка + фото', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);