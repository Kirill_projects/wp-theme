<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
    'title'         => array(
		'label' => 'Заголовок',
		'type'  => 'text',
	),
	
	'text1'       => array(
		'label' => 'Текст',
		'type'  => 'textarea',
	),

	'btn_label' => array(
		'label' => 'Текст кнопки',
		'type'  => 'text',
	),

	'btn_url' => array(
		'label' => 'URL кнопки',
		'type'  => 'text',
	),

    'img' => array(
		'label' => 'N1 Изображение (слева)',
		'type'  => 'upload',
	),
	'img2' => array(
		'label' => 'N2 Изображение (центр)',
		'type'  => 'upload',
	),
	'img3' => array(
		'label' => 'N3 Изображение (справа)',
		'type'  => 'upload',
	),
);