<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Изображение', 'fw' ),
		'description' => __( 'Адаптивное изображение на всю щирину', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);