<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
    'title'         => array(
		'label' => 'Заголовок',
		'type'  => 'text',
	), 
    'img' => array(
				'label' => 'Изображение',
				'type'  => 'upload',
	),
    'img_mob' => array(
				'label' => 'Изображение  (мобильная версия)',
				'type'  => 'upload',
	) 
);