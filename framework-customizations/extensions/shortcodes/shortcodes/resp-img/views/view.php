<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'resp-img-' );

$img  = !empty($atts['img']['url'])
			? $atts['img']['url']
			: false;
$img_mob  = !empty($atts['img_mob']['url'])
			? $atts['img_mob']['url']
			: $img;
 
if(!empty($atts['title'])){
?><section class="container-fluid post-header">            
            <h2 class="small-header text-uppercase"><?php
                echo $atts['title']; 
            ?></h2>
            <hr class='small-header__line top-header__line'>    
</section><?php } ?>    
<section class="container mt40">
    <div class="row">
        <div class="col-lg-12">

            <img src="<?php echo $img; ?>"  data-src-mobile="<?php echo $img_mob; ?>"  class="responsive" alt="">    
        </div>
    </div>
</section>