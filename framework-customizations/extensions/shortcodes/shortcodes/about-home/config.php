<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'О нас (главная страница)', 'fw' ),
		'description' => __( '2 изображения и текст', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);