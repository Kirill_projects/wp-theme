<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
    'title'         => array(
		'label' => 'Заголовок',
		'type'  => 'text',
	),
	'text'       => array(
		'label' => 'Текст',
		'type'  => 'textarea',
	),
    'img' => array(
		'label' => '№1 Изображение (слева)',
		'type'  => 'upload',
	),

	'img2' => array(
		'label' => '№2 Изображение (справа)',
		'type'  => 'upload',
	),

	'facebook_label'       => array(
		'label' => 'Facebook label',
		'type'  => 'text',
	),

	'instagram_label'       => array(
		'label' => 'Instagram label',
		'type'  => 'text',
	),

	'btn_label'       => array(
		'label' => 'Текст кнопки',
		'type'  => 'text',
	),

	'btn_url'       => array(
		'label' => 'URL кнопки',
		'type'  => 'text',
	)
);