<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'about-' );

$img  = !empty($atts['img']['url'])
			? $atts['img']['url']
			: false;

$img2  = !empty($atts['img2']['url'])
    ? $atts['img2']['url']
    : false;
?>

<section class="c-info l-index">
        <div class="c-info__container">
            <div class="c-info__half c-info__half--mobile">
                <div class="u-expander u-expander--info"></div>
                <div class="c-info__media">
                    <img src="<?php echo $img; ?>" alt="<?php echo $atts['title']; ?>">
                </div>
                <div class="c-info__media c-info__media--transformed">
                    <img src="<?php echo $img2; ?>" alt="<?php echo $atts['title']; ?>">
                </div>
            </div>
            <div class="c-info__half c-info__half--right">
                <?php if(!empty($atts['title'])){ ?>
                 <h3 class="c-info__title"><?php echo $atts['title'];?></h3>
                <?php } ?>
                <div class="c-info__content"><?php echo $atts['text'];?></div>
                <div class="c-social">
                    <a href="<?=fw_get_db_settings_option('fb-link')?>" class="c-social__item c-social__item--fb" target="_blank">
                        <img class="c-social__img" src="<?php echo get_template_directory_uri(); ?>/images/facebook-logo-button-1.svg">
                        <div class="c-social__label"><?php echo $atts['facebook_label'];?></div>
                    </a>
                    <a href="<?=fw_get_db_settings_option('insta-link')?>" class="c-social__item c-social__item--insta" target="_blank">
                        <img class="c-social__img" src="<?php echo get_template_directory_uri(); ?>/images/instagram-logo.svg">
                        <div class="c-social__label"><?php echo $atts['instagram_label'];?></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="c-btn-wrap c-btn-wrap--more u-center">
            <a href="<?php echo get_site_url().'/'.$atts['btn_url']; ?>" class="o-primary-btn o-primary-btn--more"><?php echo $atts['btn_label'];?></a>
        </div>
    </section>