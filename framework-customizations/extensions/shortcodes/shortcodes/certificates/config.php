<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Сертификаты', 'fw' ),
		'description' => __( '2 изображения по 50%', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);