<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
    'title'         => array(
		'label' => '№1 Заголовок',
		'type'  => 'text',
	),

    'img' => array(
				'label' => '№1 Изображение (слева)',
				'desc'  => 'Рекомендованный размер 720px x 490px.',
				'type'  => 'upload',
	),

	'link' => array(
				'label' => 'Ссылка',
				'desc'  => 'Ссылка изображения №1',
				'type'  => 'text',
	),
    'title2'       => array(
		'label' => '№2 Заголовок',
 
		'type'  => 'text',
	),

	'img2' => array(
				'label' => '№2 Изображение (справа)',
				'desc'  => 'Рекомендованный размер 720px x 490px.',
				'type'  => 'upload',
	),

	'link2' => array(
				'label' => 'Ссылка',
				'desc'  => 'Ссылка изображения №2',
				'type'  => 'text',
	),
    
);