<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'certificates' );

$img  = !empty($atts['img']['url'])
			? $atts['img']['url']
			: false;

$img2  = !empty($atts['img2']['url'])
	? $atts['img2']['url']
	: false;
?>   
<a href="<?php echo $atts['link']; ?>" class="gender-img-wraper gender-img-wraper--first js-gender-first">
    <?php if(!empty($atts['title'])){ ?>
    <h3 class="gender-title"><?php echo $atts['title']; ?></h3>
    <?php } ?>            
    <img src="<?php echo $img;?>">
</a>
<a href="<?php echo $atts['link2']; ?>" class="gender-img-wraper gender-img-wraper--last js-gender-last">
	<?php if(!empty($atts['title'])){  ?>
    <h3 class="gender-title"><?php echo $atts['title2']; ?></h3>
    <?php } ?>
    <img src="<?php echo $img2;?>">
</a>