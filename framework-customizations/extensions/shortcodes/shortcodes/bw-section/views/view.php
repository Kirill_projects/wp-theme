<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'bw-section-' );

$img  = !empty($atts['img']['url'])
			? $atts['img']['url']
			: false;
$img_mob  = !empty($atts['img_mob']['url'])
			? $atts['img_mob']['url']
			: $img;
 
if(!empty($atts['title'])){
?><section class="container-fluid post-header">            
            <h2 class="small-header text-uppercase"><?php
                echo $atts['title']; 
            ?></h2>
            <hr class='small-header__line top-header__line'>    
</section><?php } ?>
<section class="container"><div class="row"><div class="col-lg-12">
<?php
echo do_shortcode($atts['text']); 
?>
</div></div></section>  