<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Произвольный контент', 'fw' ),
		'description' => __( 'Заголовок + Произвольный контент', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);