<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
    'title'         => array(
		'label' => 'Заголовок',
	 
		'type'  => 'text',
	),
    'text'       => array(
		'label' => 'Содержание',
		'type'  => 'wp-editor',
         'size' => 'large', // small, large
        'editor_height' => 500,
        'wpautop' => true,
	),
    
);