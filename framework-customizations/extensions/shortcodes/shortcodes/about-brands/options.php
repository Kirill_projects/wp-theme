<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
    'title'         => array(
		'label' => 'Заголовок',
	
		'type'  => 'text',
	),

    'text'       => array(
		'label' => 'Текст',
		'type'  => 'wp-editor',
         'size' => 'small', // small, large
        'editor_height' => 400,
        'wpautop' => true,
	),
    'img' => array(
		'label' => '№1 Изображение (центр)',
		'type'  => 'upload',
	),

	'btn_label' => array(
		'label' => 'Текст кнопки',
		'type'  => 'text',
	),

	'btn_url' => array(
		'label' => 'URL кнопки',
		'type'  => 'text',
	),
    
);