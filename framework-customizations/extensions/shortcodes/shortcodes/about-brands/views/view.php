<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'about-brands-' );

$img  = !empty($atts['img']['url'])
	? $atts['img']['url']
    : false;
?>
<section class="l-index l-brand">
    <div class="c-import">
        <div class="c-import__desc-wrap c-import__desc-wrap--default">
            <div class="c-import__desc"><?php echo $atts['text']; ?></div>
            <a href="<?php echo get_site_url().'/'.$atts['btn_url']; ?>" class="c-import__more"><?php echo $atts['btn_label']; ?></a>
        </div>
        <img src="<?php echo $img;?>" class="c-import__img" alt="<?php echo $atts['title']; ?>">
    </div>
    <div class="c-import__left">
        <?php if(!empty($atts['title'])){ ?>
            <h3 class="c-import__title"><?php echo $atts['title']; ?></h3>
        <?php } ?>
        <div class="c-import__desc-wrap  c-import__desc-wrap--mobile">
            <div class="c-import__desc"><?php echo $atts['text']; ?></div>
            <a href="<?php echo get_site_url().'/'.$atts['btn_url']; ?>" class="c-import__more"><?php echo $atts['btn_label']; ?></a>
        </div>
    </div>
    <div class="orange-rectangle"></div>            
</section>