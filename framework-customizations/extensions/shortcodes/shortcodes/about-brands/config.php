<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'О наших брендах', 'fw' ),
		'description' => __( 'Изображение с текстом', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);