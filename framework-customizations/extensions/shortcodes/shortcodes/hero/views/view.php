<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'bw-content-atom-' );
$img  = !empty($atts['img']['url'])
			? $atts['img']['url']
			: false;

$img_mob  = !empty($atts['img_mob']['url'])
? $atts['img_mob']['url']
: false;
	
?>

 <div class="c-hero">
    <div class="c-hero__media">
        <img class="c-hero__img" srcset="<?php echo $img; ?>  1920w, <?php echo $img_mob;?> 768w" src="<?php echo $img_mob; ?>" alt="<?php echo $atts['title']; ?>">
        <img class="c-hero__img c-hero__img--mobile" src="<?php echo $img_mob; ?>" alt="<?php echo $atts['title']; ?>">
    </div>
    <div class="o-hero-title o-hero-title--desktop">
        <h1 class="o-hero-title__main"><?php echo  $atts['title']; ?></h1>
        <div class="o-hero-title__subtitle"><?php echo  $atts['subtitle']; ?></div>
    </div>
</div>
<div class="o-hero-title o-hero-title--mobile">
    <h1 class="o-hero-title__main"><?php echo  $atts['title']; ?></h1>
    <div class="o-hero-title__subtitle o-hero-title__subtitle--mobile"><?php echo  $atts['subtitle']; ?></div>
</div>