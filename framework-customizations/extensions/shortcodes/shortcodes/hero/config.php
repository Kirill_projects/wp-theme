<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Hero', 'fw' ),
		'description' => __( 'Hero Блок', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);