<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( '2col-img-' );

?>
<div class="c-lookbook l-lookbook">
    <?php if($atts['title']): ?>
        <div class="w-clearfix">
            <div class="c-lookbook_text">
                <?php echo $atts['title']; ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="c-lookbook__row c-lookbook__row--<?php echo $atts['direction'];?>">
        <div class="c-lookbook__item">
            
        </div>
    </div>
</div>