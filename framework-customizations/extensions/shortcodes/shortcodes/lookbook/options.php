<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	
	'title'   => array(
		'label' => 'Должность',
		'type'  => 'text'
	),

	'title_provide'   => array(
		'label' => 'Мы предоставляем',
		'type'  => 'wp-editor'
	),

	'title_req'   => array(
		'label' => 'Обязанности',
		'type'  => 'wp-editor'
	),

	'title_desc'   => array(
		'label' => 'Кого мы ищем',
		'type'  => 'wp-editor'
	),

	'btn_url'   => array(
		'label' => 'URL кнопки',
		'type'  => 'text',
	),
	'btn_label'   => array(
		'label' => 'Текст кнопки',
		'type'  => 'text',
	)
);
?>

