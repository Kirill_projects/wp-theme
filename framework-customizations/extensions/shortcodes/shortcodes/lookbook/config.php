<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Вакансия', 'fw' ),
		'description' => __( 'Требования', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);