<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(

	'text' => array(
				'label' => 'Текст',
				'desc' => 'Текст для изображения',
				'type'  => 'textarea',
	),
    'img' => array(
				'label' => 'Изображение c текстом',
				'desc' => 'Изображение с текстом на всю ширину экрана',
				'type'  => 'upload',
	)
);