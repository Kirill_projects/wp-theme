<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Изображение с текстом', 'fw' ),
		'description' => __( 'Адаптивное изображение с текстом на всю ширину', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);