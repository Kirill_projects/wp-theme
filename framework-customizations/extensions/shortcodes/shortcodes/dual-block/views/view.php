<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'img_text-' );

$img  = !empty($atts['img']['url'])
			? $atts['img']['url']
			: false;
?><section class="container-fluid post-header">            
            <h2 class="small-header text-uppercase">
               
            </h2>
            <hr class='small-header__line top-header__line'>    
</section>  
<section class="container mt40">
    <div class="row">
        <div class="col-lg-12">
            <style>
                .full-width {
                  width: 100vw;
                  position: relative;
                  left: 50%;
                  right: 50%;
                  margin-left: -50vw;
                  margin-right: -50vw;
                }

                .full-width img {
                    width:100%;
                }
            </style>
            <div class="full-width">
              <figure >
                  <img src="<?php echo $img; ?>" class="responsive">    
              </figure>
              <span><?php echo $atts['text']; ?></span>
            </div>
        </div>
    </div>
</section>