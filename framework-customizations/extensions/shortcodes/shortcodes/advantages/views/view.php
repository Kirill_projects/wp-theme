<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'advantages-' );
?><div class="landing-icon sctn-white">
    <div class="landing-cntr">
      <div class="landing-cntr clearfix">
<?php foreach ($atts['steps'] as $step){ 
 $icon  = !empty($step['list_image']['url'])
												? $step['list_image']['url']
												: fw_get_framework_directory_uri('/static/img/no-image.png');
?>     
      <div class="landing-items">
          <div class="landing-icon-wrapper">
            <img class="landing-item-icon" src="<?=$icon?>">
          </div>
          <h3><?=$step['list_heading']?></h3>
          <div><?=$step['list_text']?></div>
        </div>
     <?php } ?> 
  </div>
  

     <?php
    $atts['btn_url'] = (!empty($atts['btn_url']))?$atts['btn_url']:fw_get_db_settings_option('quote-link');
    if (!empty($atts['btn_text'])): 
    ?><a class="btn-xl cta-prefooter form-btn" href="<?php echo $atts['btn_url']; ?>"><?php echo $atts['btn_text'];  ?></a><?php endif; ?>
    </div>
    </div>
  
 