<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
/*	'title'         => array(
		'label' => 'Header',
		'desc'  => 'Section Header',
		'type'  => 'text',
	),
	'subtitle'         => array(
		'label' => 'Subheader',
		'desc'  => 'Section Subheader',
		'type'  => 'text',
	),*/
	'btn-text'         => array(
		'label' => 'Button Text',
		'desc'  => 'If empty - > No Button',
		'type'  => 'text',
		'value' => 'Get a quote',
	),
	'btn-url'         => array(
		'label' => 'Button Link',
		'desc'  => 'If empty - > global get a quote link',
		'type'  => 'text',
	),
	'steps' => array(
		'label'         => 'Features',
		'popup-title'   => 'Add feature',
		'desc'          => 'Number of list items must be 4, 8, 12 ...',
		'type'          => 'addable-popup',
		'template'      => '<img src="{{=list_image.url}}" style="width:80px; height:80px; display:block;">{{=list_heading}}',
		'popup-options' => array(
			'list_image' => array(
				'label' => 'Image',
				'desc'  => 'Recomended: SVG',
				'type'  => 'upload',
			),
			'list_heading'   => array(
				'label' =>  'Heading',
				'type'  => 'text',
			) ,
			'list_text'   => array(
				'label' =>  'Text',
				'type'  => 'textarea',
			) 
			
			
		)
	)
);