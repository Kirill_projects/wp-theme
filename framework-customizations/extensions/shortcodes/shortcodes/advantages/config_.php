<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Landing page: Advantages', 'fw' ),
		'description' => __( 'Block with header and four iconized blocks with text and call-to-action button', 'fw' ),
		'tab'         => __( 'MacBack.US', 'fw' ),
	)
);