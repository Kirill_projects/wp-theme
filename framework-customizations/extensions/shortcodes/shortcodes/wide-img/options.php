<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
    'img' => array(
				'label' => 'Изображение',
				'desc' => 'Изображение на всю ширину экрана',
				'type'  => 'upload',
	)
);