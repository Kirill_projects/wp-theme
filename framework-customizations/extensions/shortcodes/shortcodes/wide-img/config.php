<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Изображение на всю ширину', 'fw' ),
		'description' => __( 'Адаптивное изображение на всю ширину', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);