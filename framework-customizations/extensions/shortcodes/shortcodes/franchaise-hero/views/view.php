<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'hero-franchise' );
$img  = !empty($atts['img']['url'])
			? $atts['img']['url']
			: false;

$img_mob  = !empty($atts['img_mob']['url'])
? $atts['img_mob']['url']
: false;
	
?>


<div class="c-hero c-hero--lookbook">
    <img class="c-book-hero__img" src="<?php echo $img;?>" alt="<?php echo $atts['title']; ?>">
    <div class="c-book-title">
        <span class="c-book-hero__label">
            <?php echo str_replace('||', '<br>', preg_replace('/\|([^|]+)\|/', '<span>\\1</span>', $atts['title']));
                            ?>
                                
                            </span>
        <h1 class="c-book-hero__header"><?php echo str_replace('||', '<br>', preg_replace('/\|([^|]+)\|/', '<span>\\1</span>', $atts['subtitle'])); 
                            ?>                 
        </h1>
    </div>
</div>

<!-- <a href="<?php echo $atts['file']['url'];?>"><?php echo $atts['file'];?></a></div>
  <span><?php echo size_format(filesize( get_attached_file( $atts['file']['attachment_id']))); ?></span> -->