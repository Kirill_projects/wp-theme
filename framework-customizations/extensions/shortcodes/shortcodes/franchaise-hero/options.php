<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'         => array(
		'label' => 'Заголовок',
		'type'  => 'text',
		'desc'  => 'Используйте "|" для |выделения| текста и переноса || слов в мобильной версии. Всегда используйте парное число спецсимволов.'
	),

	'subtitle' => array(
			'label' => 'Текст',
			'type'  => 'textarea',
	),

	'img' => array(
		'label' => 'Изображение',
		'type'  => 'upload',
	),

	'img_mob' => array(
		'label' => 'Мобильное изображение',
		'type'  => 'upload',
	),

	'file_status' => array(
		'label' => 'Статус кнопки',
		'type'  => 'select',
		'choices' => array(
            'true'  => __('Активна'),
            'false'  => __('Неактивна')
        ),
	),

	'file'       => array(
		'label' => 'PDF Файл',
		'type'  => 'upload',
	)
);