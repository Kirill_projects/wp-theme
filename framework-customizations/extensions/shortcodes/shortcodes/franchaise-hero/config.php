<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
    'page_builder' => array(
		'title'       => __( 'Hero-Franchise', 'fw' ),
		'description' => __( 'Hero-Franchise Блок', 'fw' ),
		'tab'         => __( 'Covrigo', 'fw' ),
	)
);