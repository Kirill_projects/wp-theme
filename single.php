<?php
/**
 * The Template for displaying all single posts
 */

get_header(); 
$next_post_url = (get_adjacent_post(false,'',false))?get_permalink( get_adjacent_post(false,'',false)->ID ):false;
$previous_post_url = (get_adjacent_post(false,'',true))?get_permalink( get_adjacent_post(false,'',true)->ID ):false;
		if ( have_posts() ) :
				while ( have_posts() ) : the_post();	
?>
  <section class="container post">
      <h2 class="big-header__green text-uppercase big-header__center">
      
          <img src="<?php echo get_template_directory_uri(); ?>/img/header-line-left.png" alt="">
         <?php pll_e('News'); ?>
          <img src="<?php echo get_template_directory_uri(); ?>/img/header-line-right.png" alt="">
      </h2>
      <div class="row post-item">
         <div class="col-lg-12"> 
             <img src="<?php the_post_thumbnail_url('news-big')?>" alt="" class='post-item__image'> 
             <h1 class="post-item__header">
                <?php the_title(); ?>
             </h1>
             <div class="post-item__text">
                 <?php the_content(); ?>
                 <div class="article-date">
				<span><?=get_the_date()?></span>
			</div>
             </div>   
         </div>      
      </div>
  </section> 				 
<?php
		endwhile;
			endif;
get_footer();
