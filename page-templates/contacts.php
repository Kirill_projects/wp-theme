<?php
/**
 * Template Name:  Контакты
 */
get_header(); ?>
			<!-- if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					 the_title( '<h1 class="entry-title">', '</h1>' );
					 the_content();  
				endwhile;
			endif; -->
	<div class="l-about">
        <div class="l-container l-container--about">
            <div class="o-page-header-wrap">
                <?php echo str_replace(' | ', '<br />', the_title( '<h1 class="o-page-header">', '</h1>' )); ?>
            </div>
            <div class="o-page-subtitle">
                <div class="o-page-subtitle__content"><?php echo fw_get_db_post_option(get_the_ID(), 'subtitle');?></div>
            </div>
            <div class="l-about-wrap">
                <div class="l-about__content">
                    <?php 
                        if ( have_posts() ) :
                            while ( have_posts() ) : the_post();
                                 the_content();  
                            endwhile;
                        endif;
                    ?>
                </div>

        
                <div class="sctn-map">
                    <div class="o-page-header-wrap">
                        <h3 class="o-map-header"><?php pll_e('MAP_TITLE');?></h3>
                    </div>
                    <div id="map" class="c-map"></div>
                </div>
                <div class="c-half-img">
                    <div class="o-page-header-wrap">
                        <h3 class="o-map-header"><?php pll_e('CERTIFICATES_TITLE');?></h3>
                    </div>
                    <div class="c-half-img__list">
                        <?php foreach (fw_get_db_settings_option('gallery') as $key => $value) { ?>
                            <figure class="c-half__item">
                                <img src="<?php echo $value['url']; ?>" alt="<?php pll_e('CERTIFICATES_TITLE');?>">  
                            </figure>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="u-push"></div>
    </div>

<?php 
    $map = fw_get_db_settings_option('map_center');
    if(!empty($map)) { 
        $map_center = explode(':',fw_get_db_settings_option('map_center'));
        $salon_marker = explode(':',fw_get_db_settings_option('salon_pin'));
        $outlet_marker = explode(':',fw_get_db_settings_option('outlet_pin'));
        $store_marker = explode(':',fw_get_db_settings_option('store_pin'));
    }        
?> 

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgHDGM_mBIupRI3FBwklVLf9kl2OFuHO0"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/custom-marker.js"></script>
<script>
        jQuery(document).ready(function($) {

            var salon_marker = {lat: '<?php echo $salon_marker[0]; ?>', lng: '<?php echo $salon_marker[1]; ?>'},
                outlet_marker = {lat: '<?php echo $outlet_marker[0]; ?>', lng: '<?php echo $outlet_marker[1]; ?>'},
                store_marker = {lat: '<?php echo $store_marker[0]; ?>', lng: '<?php echo $store_marker[1]; ?>'},
                contactMapLatLng =  {lat: '<?php echo $map_center[0]; ?>', lng: '<?php echo $map_center[1]; ?>'}; 

            var aboutModule = (function() {

                var template=[],
                stores = [
                {
                    position:salon_marker,
                    title:'<?php pll_e('SALON'); ?>',
                    address:'<?=nl2br(fw_get_db_settings_option('salonaddress_'.pll_current_language()))?>',
                    hourTitle:'<?php pll_e('HOURS'); ?>',
                    hours:'<?=nl2br(fw_get_db_settings_option('salon_workinghours_'))?>',
                    status:'is-active'
                },

                {
                    position:store_marker,
                    title:'<?php pll_e('STORE'); ?>',
                    address:'<?=nl2br(fw_get_db_settings_option('streetaddress_'.pll_current_language()))?>',
                    hourTitle:'<?php pll_e('HOURS'); ?>',
                    hours:'<?=nl2br(fw_get_db_settings_option('store_workinghours_'))?>',
                    status:''
                },
                
                {
                    position:outlet_marker,
                    title:'<?php pll_e('OUTLET'); ?>',
                    address:'<?=nl2br(fw_get_db_settings_option('outletaddress_'.pll_current_language()))?>',
                    hourTitle:'<?php pll_e('HOURS'); ?>',
                    hours:'<?=nl2br(fw_get_db_settings_option('outlet_workinghours_'))?>',
                    status:''
                }
                ];

                return {

                    init:function() {
                        this.initMap();
                    },

                    drawTooltips:function() {

                        for (prop in stores) {
                            template.push('<div class="c-opening '+stores[prop].status+'" data-marker="'+stores[prop].position.lat+'">'+
                                '<div class="c-opening__item">'+
                                    '<span class="c-opening__header c-opening__header--upper">'+ stores[prop].title +'</span>'+
                                    '<address class="c-opening__info">'+ stores[prop].address +'</address>'+
                                '</div>'+
                                '<div class="c-opening__item">'+
                                    '<span class="c-opening__header">'+ stores[prop].hourTitle +'</span>'+
                                    '<span class="c-opening__info">'+ stores[prop].hours +'</span>'+
                                '</div>'+
                            '</div>'
                            ); 
                        }
                    },
 
                    makeInfoWindowEvent:function(map, infowindow, marker) {

                        google.maps.event.addListener(marker, 'click', function() {
                            $('.c-opening').removeClass('is-active');
                            //console.log($('.c-opening[data-marker='"+marker.getPosition().lat()+"']'))
                            $(".c-opening[data-marker='"+marker.getPosition().lat()+"']").addClass('is-active');
                        });

                        google.maps.event.addListener(map, "click", function(event) {
                           $('.c-opening').removeClass('is-active');
                        });
                    },

                    initMap:function() {
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 13,
                            center: {lat:parseFloat(contactMapLatLng.lat), lng:parseFloat(contactMapLatLng.lng)}
                        });

                        this.drawTooltips();

                        for (var i = 0; i < stores.length; i++) {

                            var marker = new google.maps.Marker({
                                position: new google.maps.LatLng(stores[i].position.lat, stores[i].position.lng),
                                map: map
                            });
                            this.makeInfoWindowEvent(map, template[i], marker);
                            new CustomMarker(new google.maps.LatLng(stores[i].position.lat, stores[i].position.lng),map,template[i])
                        }
                       /* var myMarker = new CustomMarker(myLatLng,map,template);*/
                    }
                }
            })();
            aboutModule.init();
        });
    </script>
<?php  get_footer();?>
