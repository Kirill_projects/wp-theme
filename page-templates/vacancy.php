<?php
/**
 * Template Name: Вакансия
 */
get_header(); 
?>


 <div class="l-container l-container--brands">
    <div class="o-page-header-wrap">
    <?php the_title( '<h1 class="o-page-header">', '</h1>' ); ?>
    </div>
    <div class="o-page-subtitle">
    <div class="o-page-subtitle__content">
    <?php echo fw_get_db_post_option(get_the_ID(), 'subtitle');?>
    </div>
    </div>   
    <div class="brand-list">  
    <?php 

    if ( have_posts() ) :
        while ( have_posts() ) : the_post();
        the_content(); 
        endwhile;
    endif;  
    ?>
    </div>
    <div class="u-push"></div>
</div> 
<?php get_footer();?>
