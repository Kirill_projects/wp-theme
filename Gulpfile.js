'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');
var concat = require('gulp-concat'); 

gulp.task('sass', function () {
  gulp.src('./sass/**/*.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('jsmin', function () {
    gulp.src('./js/*.js')
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('js/min')); 
});

gulp.task('scripts', function() {
  return gulp.src('./js/min/*.js')
    .pipe(concat('all.js', { newLine: ';', stat: { mode: '0775' } }))
    .pipe(gulp.dest('./'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.sass', ['sass']);
  gulp.watch('./js/*.js', ['jsmin', 'scripts']);
});

