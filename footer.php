<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 */
/*$logo_footer = fw_get_db_settings_option( 'logo_footer' );
$logo_footer = !empty( $logo_footer[ 'url' ] ) ? $logo_footer[ 'url' ] : false;*/

?>
<footer class="c-footer">
    <div class="c-footer-social">
        <div class="c-footer-social__row  c-footer-social__row--address">
            <div class="footer-social__title">
                <?php pll_e('SALON'); ?>
            </div>
            <a href="#" class="c-footer-social__link">
                <?=nl2br(fw_get_db_settings_option('salonaddress_'.pll_current_language()))?>
            </a>
            <div class="footer-social__title">
                <?php pll_e('OUTLET'); ?>
            </div>
            <a href="#" class="c-footer-social__link">
                <?=nl2br(fw_get_db_settings_option('outletaddress_'.pll_current_language()))?>
            </a>
            <div class="footer-social__title">
                <?php pll_e('STORE'); ?>
            </div>
            <a href="#" class="c-footer-social__link">
                <?=nl2br(fw_get_db_settings_option('streetaddress_'.pll_current_language()))?>
            </a>
        </div>
        <div class="c-footer-social__row c-footer-social__row--phones">
            <div class="footer-social__title">
                <?php pll_e('PHONES'); ?>
            </div>
            <?php 
            
            $phones = array_map(function($item){ return trim($item); }, explode(',',fw_get_db_settings_option('phone')));

            foreach($phones as $phn){
                ?>
            <a href="tel:<?=$phn?>" class="c-footer-social__link">
                <?=$phn?>
            </a>
            <?php
            }
            ?>
        </div>
        <div class="c-footer-social__row c-footer-social__row--pt">
            <a href="<?=fw_get_db_settings_option('fb-link')?>" class="c-footer-social__item" target="_blank"> 
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30" version="1.1">
                    <g transform="translate(1865 -8295)">
                    <g >
                    <use xlink:href="#path0_fill" transform="translate(-1865 8295)" fill="#f1c499"/>
                    </g>
                    </g>
                    <defs>
                    <path id="path0_fill" d="M 15 0C 6.72903 0 0 6.72903 0 15C 0 23.2704 6.72903 30 15 30C 23.2704 30 30 23.2704 30 15C 30 6.72903 23.2716 0 15 0ZM 18.7304 15.5281L 16.29 15.5281C 16.29 19.427 16.29 24.2262 16.29 24.2262L 12.6738 24.2262C 12.6738 24.2262 12.6738 19.4735 12.6738 15.5281L 10.9548 15.5281L 10.9548 12.4539L 12.6738 12.4539L 12.6738 10.4654C 12.6738 9.04133 13.3505 6.81604 16.3232 6.81604L 19.0029 6.82631L 19.0029 9.81048C 19.0029 9.81048 17.3745 9.81048 17.0579 9.81048C 16.7413 9.81048 16.2912 9.96878 16.2912 10.6479L 16.2912 12.4545L 19.0464 12.4545L 18.7304 15.5281Z"/>
                    </defs>
                    </svg>
            </a>
            <a href="<?=fw_get_db_settings_option('insta-link')?>" class="c-footer-social__item" target="_blank"> 

                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30" version="1.1">
                    <g  transform="translate(1805 -8295)">
                    <g >
                    <g >
                    <use xlink:href="#path0_i_fill" transform="translate(-1793 8307)" fill="#f1c499"/>
                    </g>
                    <g id="Vector">
                    <use xlink:href="#path1_i_fill" transform="translate(-1786.34 8303.43)" fill="#f1c499"/>
                    </g>
                    <g id="Vector">
                    <use xlink:href="#path2_i_fill" transform="translate(-1805 8295)" fill="#f1c499"/>
                    </g>
                    <g id="Vector">
                    <use xlink:href="#path3_i_fill" transform="translate(-1796.87 8308.25)" fill="#f1c499"/>
                    </g>
                    </g>
                    </g>
                    <defs>
                    <path id="path0_i_fill" d="M 3.00169 6.0058C 4.65661 6.0058 6.0058 4.65903 6.0058 3.0029C 6.0058 2.34915 5.79191 1.74555 5.43785 1.25252C 4.89225 0.496053 4.00528 5.43947e-07 3.00351 5.43947e-07C 2.00113 5.43947e-07 1.11476 0.495448 0.567955 1.25191C 0.212683 1.74494 0.000606243 2.34855 2.03796e-06 3.0023C -0.00181058 4.65842 1.34617 6.0058 3.00169 6.0058Z"/>
                    <path id="path1_i_fill" d="M 2.89233 2.89173L 2.89233 0.374607L 2.89233 2.30486e-08L 2.51591 0.00120835L 4.51753e-07 0.00906331L 0.00966703 2.90139L 2.89233 2.89173Z"/>
                    <path id="path2_i_fill" d="M 15 0C 6.72903 0 0 6.72903 0 15C 0 23.2704 6.72903 30 15 30C 23.2704 30 30 23.2704 30 15C 30 6.72903 23.2716 0 15 0ZM 23.5308 13.2496L 23.5308 20.2342C 23.5308 22.0535 22.0517 23.532 20.2336 23.532L 9.76637 23.532C 7.94772 23.532 6.46923 22.0535 6.46923 20.2342L 6.46923 13.2496L 6.46923 9.76698C 6.46923 7.94832 7.94772 6.46983 9.76637 6.46983L 20.233 6.46983C 22.0517 6.46983 23.5308 7.94832 23.5308 9.76698L 23.5308 13.2496Z"/>
                    <path id="path3_i_fill" d="M 11.5355 1.75038C 11.5355 4.32248 9.44252 6.41666 6.86921 6.41666C 4.2959 6.41666 2.20354 4.32248 2.20354 1.75038C 2.20354 1.13168 2.3268 0.54016 2.54673 5.16289e-07L -1.33682e-07 5.16289e-07L -1.33682e-07 6.98461C -1.33682e-07 7.88729 0.732901 8.61838 1.63498 8.61838L 12.1016 8.61838C 13.0025 8.61838 13.7366 7.88729 13.7366 6.98461L 13.7366 5.16289e-07L 11.1887 5.16289e-07C 11.4104 0.54016 11.5355 1.13168 11.5355 1.75038Z"/>
                    </defs>
                    </svg>
            </a>
        </div>
        <div class="c-footer-social__cpy">&copy;
            <?=date('Y')?>
            <?=fw_get_db_settings_option('text-copy_'.pll_current_language())?>

        </div>
    </div>
</footer> <?php
wp_footer();?>
<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110084435-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110084435-1');
</script>
<?php endif; ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46769928 = new Ya.Metrika({
                    id:46769928,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/46769928" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<!-- <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.zoom.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/cf7.js"></script>

    <script>
        var productModule =(function() {
            var self= this;
            var $product_popup = jQuery('.c-product-popup');
            var $backdrop = jQuery('#backdrop');
            
            return {
                
                init:function() {
                    this.bindEvents();
                    this.initGallery('product-gallery');
                    this.initZoom();
                    this.fillEmailData();
                    this.disableScroll();
                },

                initGallery:function (el) {
                    jQuery('#'+el).slick({
                        infinite: true,
                        dots:true,
                        arrows: true,
                        verticalSwiping:false,
                        prevArrow:'<div class="o-arrow o-arrow--prev"><span class="o-arrow__item o-arrow__item--prev"></span></div>',
                        nextArrow:'<div class="o-arrow o-arrow--next"><span class="o-arrow__item o-arrow__item--next"></span></div>'
                    });
                },

                initZoom:function() {
                    if(jQuery(window).width() > 1024) {
                        jQuery('.js-zoom').each(function(i, object) {
                            jQuery(this).zoom({url: jQuery(this).data('href')});
                        });
                    }
                },

                bindEvents:function() {
                    var self = this;

                    jQuery(document).keyup(function(e) {
                        if (e.keyCode == 27) { // Esc
                            productModule.closePopup(jQuery(this));  
                        }
                    });
                    jQuery('.js-product').on('click', function() {
                        self.openPopup(jQuery(this));
                    }); 

                    jQuery('.js-close').on('click', function() {
                        self.closePopup(jQuery(this));     
                    });

                    jQuery('.js-popup').on('click', function() {
                        self.openPopup(jQuery(this).data('href'));       
                    });

                    jQuery(".wpcf7").on('wpcf7:mailsent', function(event){

                        jQuery('.wpcf7').addClass('is-deactivated');
                        setTimeout(function() {
                            jQuery('.wpcf7').hide();
                            jQuery('.js-status').text(jQuery('.wpcf7-response-output').text());
                            jQuery('.c-reserve-status').addClass('is-active');
                        },500);
                        setTimeout(function() {
                            self.closePopup(); 
                            setTimeout(function() {
                                jQuery('.wpcf7').show();
                                jQuery('.wpcf7').removeClass('is-deactivated');
                                jQuery('.c-reserve-status').removeClass('is-active');
                            },900);    
                        },3000);
                    });
                },

                openPopup:function(el) {
                    jQuery(el).addClass('is-active');
                    jQuery('#backdrop').addClass('is-active');
                    jQuery('body').addClass('disable-scrolling is-active');
                },

                closePopup:function(el) {
                    jQuery('body,#backdrop,.l-popup,.c-main-nav,.c-sandwich').removeClass('is-active');
                    jQuery('body').removeClass('disable-scrolling');
                },

                fillEmailData:function() {
                    var $img = jQuery('.slick-slide img');
                    jQuery('input[name="product-img"]').val(jQuery($img[$img.length-1]).data('src'));
                    jQuery('input[name="product-title"]').val(jQuery('.js-title').text());
                    jQuery('input[name="product-href"]').val(window.location.href);
                },

                disableScroll:function() {
                    //ios popup disable scroll
                    document.ontouchmove = function ( event ) {

                        var isTouchMoveAllowed = true, target = event.target;
                        while ( target !== null ) {
                            if ( target.classList && target.classList.contains( 'disable-scrolling' ) ) {
                                isTouchMoveAllowed = false;
                                break;
                            }
                            target = target.parentNode;
                        }

                        if ( !isTouchMoveAllowed ) {
                            event.preventDefault();
                        }
                    };
                }
            }
        })();
        productModule.init();
    </script> -->
</body> 
</html>