<?php
/**
 * Template Name: News Grid Page
 */
get_header();  
 ?>
<section class="sctn-body">

		<div class="content-wrapper">
<?php if ( have_posts() ) : ?>
			<div class="h2-wrapper"><?php the_archive_title( '<h1 class="h1">', '</h1>' );
			     the_archive_description( '<div class="subtitle">', '</div>' );   ?> 
			</div>
			<section class="news-container gs-clearfix">
				
				 <?php 
				while ( have_posts() ) : the_post();	?>
                    <article class="col-3 news" itemscope itemtype="http://schema.org/NewsArticle">
					<div class="gs-hidden" itemprop="author" itemscope itemtype="https://schema.org/Organization">
						<span itemprop="name"><?=$_SERVER['HTTP_HOST']?></span>
					</div>
					<div class="news-item-container">
						<figure class="news-img-wrapper">
							<a href="<?=get_permalink()?>">
								<img class="news-img" src="<?php the_post_thumbnail_url('news-thumb')?>" itemprop="image">
								<meta itemprop="url" content="<?php the_post_thumbnail_url('news-thumb')?>">
							</a>
						</figure>
						<h3 class="h3-green" itemprop="headline">
							<a href="<?=get_permalink()?>" itemprop="url"><?php the_title(); ?></a>
						</h3>
						<div class="news-txt-wrapper">
							<div class="txt-reg" itemprop="description">
								<?php the_excerpt(); ?>
							</div>
							<div class="news-data-container gs-clearfix">
								<div class="news-date-wrapper">
									<div class="txt-small">
										<date itemprop="datePublished"><?=get_the_date()?></date>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
                    <?php endwhile; ?>
                    <!-- end of the loop -->
                <br clear="all">
                    <!-- pagination here -->
                    <?php
                      if (function_exists('custom_pagination')) {
                        custom_pagination($the_query->max_num_pages,"",$paged);
                      }
                    ?>
                
                  <?php wp_reset_postdata(); ?>
                
                  			 
			</div>
			<?php else:  ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>	
		</div>
	</section>
<?php get_footer();