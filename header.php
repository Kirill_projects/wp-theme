<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<?php
$logo_head = fw_get_db_settings_option('logo_head');
$logo_head = !empty($logo_head['url']) ? $logo_head['url'] : false;
$logo_home_head = $logo_head;
$logo_alt_head = fw_get_db_settings_option('logo_home_head');
$logo_alt_head = !empty($logo_alt_head['url']) ? $logo_alt_head['url'] : false;    
    
if(is_front_page()){
    $logo_home_head = $logo_alt_head;
}
?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php
	
	$translations = pll_the_languages(array('hide_if_no_translation'=>false, 'raw'=>1)); 
	
	$fbapp = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option( 'fbapp' ) : '';
	if ( !empty( $fbapp ) ):
		?>
	<meta content="<?php echo $fbapp ?>" property="fb:app_id"/>
	<?php endif ?>
	<title>
		<?php wp_title( '*', true, 'right' ); ?>
	</title>
	<meta content="initial-scale=1.0, user-scalable=no" name="viewport">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="<?php echo get_template_directory_uri(); ?>/images/favicon_v.png" rel="shortcut icon" type="image/x-icon">
    <link href="<?php echo get_template_directory_uri(); ?>/css/vendor/slick.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/css/main.css" rel="stylesheet" type="text/css">
    
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="backdrop"></div>
<div class="tooltip bs-tooltip-bottom bs-tooltip-bottom-docs" role="tooltip">
    <div class="arrow"></div>
    <div class="tooltip-inner">
      coming soon
    </div>
  </div>
  

    <?php 
   
   $curlang = array_filter($translations, function($lang){ return $lang['current_lang']; });
   $curlang = reset($curlang); 
   $otherlangs = array_filter($translations, function($lang){ return !$lang['current_lang']; });
    ?>
    <div class="c-logo"> 
    	<a href="<?php echo pll_home_url(); ?>" class="c-logo__item c-logo__item--desktop"> 
    		<?php if(!is_page_template('page-templates/lookbook.php')) { ?>
				<img src="<?php echo get_template_directory_uri(); ?>/images/logo-b.svg" class="c-logo__img"> 
			<?php } ?>
			<?php if(is_page_template('page-templates/lookbook.php')) { ?>
				<img src="<?php echo get_template_directory_uri(); ?>/images/logo-w.svg" class="c-logo__img"> 
			<?php } ?>
		</a> 

		<?php if(is_page_template('page-templates/lookbook.php')) { ?>
			<a href="/" class="c-logo__item c-logo__item--mobile">
		 	<img src="<?php echo get_template_directory_uri(); ?>/images/logo-mob-w.svg" class="c-logo__img">
		</a> 
		<?php } else {?>
		<a href="/" class="c-logo__item c-logo__item--mobile">
		 	<img src="<?php echo get_template_directory_uri(); ?>/images/logo-mob-b.svg" class="c-logo__img">
		</a> 
		<?php } ?>
	</div>
    <nav class="c-main-nav c-main-nav--mobile"> 
    <?php
	  wp_nav_menu( array(
				  'theme_location' => 'primary',
				  'container' => '',  
				  'items_wrap'      => '%3$s',
				  'container_class' =>'right-nav',
				  'menu_class' => 'c-main-nav__item',
				  'with_ul'=>false,
		  
				  'walker' => new footerMenuWalker ()
				) ); 
				?>  
		<div class="c-lng c-lng--mobile js-lang">
			<div class="c-lng__title" title="<?php echo $curlang['name']; ?>"><?php echo strtoupper($curlang['slug']); ?></div>
			 <div class="c-lng__container">
	           <?php 
	                foreach($otherlangs as $slug=>$lng){
	                        ?><a class="c-lng__item" href="<?php echo $lng['url']; ?>" title="<?php echo $lng['name']; ?>"><?php echo strtoupper($slug);?></a><?php
	                         
						};
				?>
				</div>
	        <img src="<?php echo get_template_directory_uri(); ?>/images/Polygon.svg"> 
	    </div>
     </nav>
     
    <div class="c-sandwich js-sandwich"> 
    	<span class="c-sandwich__top">
          <span class="c-sandwich__inner"></span>
        </span>
        <span class="c-sandwich__middle">
          <span class="c-sandwich__inner"></span>
        </span>
        <span class="c-sandwich__bottom">
          <span class="c-sandwich__inner"></span>
        </span>
    </div>
    <div style="display: none;"></div>
<header class="header <?php echo (is_front_page() ? 't-header-absolute' : '') ?><?php echo is_page_template('page-templates/lookbook.php') ? 't-header-white t-header-absolute' :'' ?>">
    <nav class="c-main-nav c-main-nav--desktop"> 
    <?php
	  wp_nav_menu( array(
				  'theme_location' => 'primary',
				  'container' => '',  
				  'items_wrap'      => '%3$s',
				  'container_class' =>'right-nav',
				  'menu_class' => 'c-main-nav__item',
				  'with_ul'=>false,
		  
				  'walker' => new footerMenuWalker ()
				) ); 
				?>  
		<div class="c-lng c-lng--mobile js-lang">
			<div class="c-lng__title" title="<?php echo $curlang['name']; ?>"><?php echo strtoupper($curlang['slug']); ?></div>
			 <div class="c-lng__container">
	           <?php 
	                foreach($otherlangs as $slug=>$lng){
	                        ?><a class="c-lng__item" href="<?php echo $lng['url']; ?>" title="<?php echo $lng['name']; ?>"><?php echo strtoupper($slug);?></a><?php
	                         
						};
				?>
				</div>
	        <img src="<?php echo get_template_directory_uri(); ?>/images/Polygon.svg"> 
	    </div>
     </nav>
  <!--   <span class="c-close-nav js-close-nav"> <img src="<?php echo get_template_directory_uri(); ?>/images/close.svg"> </span> -->
</header>
<div class="c-lng c-lng--desktop js-lang">
    <div class="c-lng__title" title="<?php echo $curlang['name']; ?>"><?php echo strtoupper($curlang['slug']); ?></div>
    <div class="c-lng__container">
       <?php 
            foreach($otherlangs as $slug=>$lng){
                    ?><a class="c-lng__item" href="<?php echo $lng['url']; ?>" title="<?php echo $lng['name']; ?>"><?php echo strtoupper($slug);?></a><?php
                     
				};
		?>
    </div>
    <img src="<?php echo get_template_directory_uri(); ?>/images/Polygon.svg"> 
</div>


 

 
 
				
 