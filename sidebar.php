<?php
/**
 * The Sidebar containing the main widget area
 */
?>
<div id="secondary">
<div class="search-container w-clearfix">
<form role="search" method="get" class="search-form" action="/">
    <div class="search-input">
   	 	<input class="search-placeholder" placeholder="Search" value="" name="s" type="search">
    </div>
    <input class="search-btn w-button" value="Search" type="submit">
</form></div>
<?php dynamic_sidebar(); ?>
</div><!-- #secondary -->