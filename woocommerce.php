<?php
/**
 * The Template for displaying all single posts
 */

get_header(); ?>
<?php woocommerce_content(); ?>

<?php
get_footer();
