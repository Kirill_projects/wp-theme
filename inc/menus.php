<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Register menus
 */
 
class mainMenuWalker extends Walker_Nav_Menu {
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
    // назначаем классы li-элементу и выводим его
   	$appcls = '';
	 $class_names ='nav-item';
    if(in_array('login-btn',$item->classes)) $appcls.='login-btn desktop '; 
	if(in_array('gs-button',$item->classes)) $appcls.='gs-button '; 
	if(in_array('current-menu-item',$item->classes) || in_array('current_page_item',$item->classes)){
		$class_names ='nav-item gs--current';
	}
	if(!empty($appcls)){$class_names = $appcls; } 
	
    $class_names = ' class="' .esc_attr( $class_names ). '"';
    $output.= '<li itemprop="name" id="menu-item-' . $item->ID . '">';

    // назначаем атрибуты a-элементу
    $attributes= !empty( $item->url ) ? ' href="' .esc_attr($item->url). '"' : '';
    $item_output = $args->before;

    // проверяем, на какой странице мы находимся
    $item_url = esc_attr( $item->url );
    $item_output.= '<a'. $attributes .' itemprop="url"' .$class_names. '>'.$item->title.'</a>';
    
	// заканчиваем вывод элемента
    $item_output.= $args->after;
    $output.= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
} 

class mobMenuWalker extends Walker_Nav_Menu {
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
    // назначаем классы li-элементу и выводим его
   	$appcls = '';
	 $class_names ='nav-item';
    if(in_array('login-btn',$item->classes)) $appcls.='login-btn mobile'; 
//	if(in_array('gs-button',$item->classes)) $appcls.='gs-button '; 
	if(in_array('current-menu-item',$item->classes) || in_array('current_page_item',$item->classes)){
		$class_names ='nav-item gs--current';
	}
	if(!empty($appcls)){$class_names = $appcls; } 
	
    $class_names = ' class="' .esc_attr( $class_names ). '"';
    $output.= '<li>';

    // назначаем атрибуты a-элементу
    $attributes= !empty( $item->url ) ? ' href="' .esc_attr($item->url). '"' : '';
    $item_output = $args->before;

    // проверяем, на какой странице мы находимся
    $item_url = esc_attr( $item->url );
    $item_output.= '<a'. $attributes .' ' .$class_names. '>'.$item->title.'</a>';
    
	// заканчиваем вывод элемента
    $item_output.= $args->after;
    $output.= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
} 

class footerMenuWalker extends Walker_Nav_Menu {
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
    // назначаем классы li-элементу и выводим его
 	$class_names = $args->menu_class;
	$li_wrap = (isset($args->with_ul) && $args->with_ul);
	$li_class = (isset($args->li_class)?' class="'.$args->li_class.'" ':'');
	  
	if(in_array('current-menu-item',$item->classes) || in_array('current_page_item',$item->classes)){
		$class_names.=' item-active';
	}
	
    $class_names = ' class="pageID-'.$item->ID .' '. esc_attr( $class_names ). '"';

    $attributes= !empty( $item->url ) ? ' href="' .esc_attr($item->url). '"' : '';
    $item_output = $args->before;

    // проверяем, на какой странице мы находимся
	
    $item_url = esc_attr( $item->url );
    $item_output.= (($li_wrap)?'<li'.$li_class.'>':'').'<a'. $attributes .$class_names.'>'. $item->title.'</a>'.(($li_wrap)?'':'');
    
	// заканчиваем вывод элемента
  //  $item_output.= $args->after;
    $output.=   $item_output ;
  }
  public function end_el( &$output, $item, $depth = 0, $args = array() ) {
      $li_wrap = (isset($args->with_ul) && $args->with_ul);
      if($li_wrap){ 
            $output .= "</li>";
          } 
    }
}
 
// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
	'primary'   => __( 'Top primary menu', 'unyson' ),
	'footer_main' => __( 'Footer Menu', 'unyson' )
) );

// add_filter( 'nav_menu_css_class', 'add_my_class_to_nav_menu', 10, 2 );
/* function add_my_class_to_nav_menu( $classes, $item ){
	 
	if(in_array('current-menu-item',$classes) || in_array('current_page_item',$classes)){
		return array('nav-item', 'gs--current');
	}
	return array('nav-item');
}*/ 