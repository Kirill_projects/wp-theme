<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Define custom posts and taxonomies
 */

/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

$labels = array(
	'name'               => __( 'Продукты', 'unyson' ),
	'singular_name'      => __( 'Продукт', 'unyson' ),
	'menu_name'          => __( 'Продукты', 'unyson' ),
	'name_admin_bar'     => __( 'Продукт', 'unyson' ),
	'add_new'            => __( 'Добавить', 'unyson' ),
	'add_new_item'       => __( 'Добавить Продукт', 'unyson' ),
	'new_item'           => __( 'Новый Продукт', 'unyson' ),
	'edit_item'          => __( 'Редактировать Продукт', 'unyson' ),
	'view_item'          => __( 'Список продуктов', 'unyson' ),
	'all_items'          => __( 'Все продукты', 'unyson' ),
	'search_items'       => __( 'Поиск продуктов', 'unyson' ),
	'parent_item_colon'  => __( 'Parent Products:', 'unyson' ),
	'not_found'          => __( 'Не найдено продуктов.', 'unyson' ),
	'not_found_in_trash' => __( 'Нет продуктов в корзине', 'unyson' )
);

$args = array(
	'labels'             => $labels,
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => array( 'slug' => 'products' ),
	'capability_type'    => 'post',
	'has_archive'        => true,
	'hierarchical'       => false,
	'menu_position'      => null,
	'taxonomies'  => array( 'category' ),
	'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
);

register_post_type( 'products', $args );

 
	$labels = array(
		'name'                  => _x( 'Упаковка', 'Post Type General Name', 'bwnuts' ),
		'singular_name'         => _x( 'Упаковка', 'Post Type Singular Name', 'bwnuts' ),
		'menu_name'             => __( 'Упаковка', 'bwnuts' ),
		'name_admin_bar'        => __( 'Упаковка', 'bwnuts' ),
		'attributes'            => __( 'Атрибуты', 'bwnuts' ),
		'all_items'             => __( 'Все упаковки', 'bwnuts' ),
		'add_new_item'          => __( 'Добавить', 'bwnuts' ),
		'add_new'               => __( 'Добавить', 'bwnuts' ),
		'new_item'              => __( 'Новый тип', 'bwnuts' ),
		'edit_item'             => __( 'Редактировать', 'bwnuts' ),
		'update_item'           => __( 'Обновить', 'bwnuts' ),
		'view_item'             => __( 'Просмотр', 'bwnuts' ),
		'view_items'            => __( 'Смотреть', 'bwnuts' ),
		'search_items'          => __( 'Поиск', 'bwnuts' ),
		'not_found'             => __( 'Не найдено', 'bwnuts' ),
		'not_found_in_trash'    => __( 'Не найдено в корзине', 'bwnuts' ),
		'featured_image'        => __( 'Изображение упаковки', 'bwnuts' ),
		'set_featured_image'    => __( 'Установить картинку', 'bwnuts' ),
		'remove_featured_image' => __( 'Удалить картинку', 'bwnuts' ),
		'use_featured_image'    => __( 'Использовать картинку', 'bwnuts' ),
		'insert_into_item'      => __( 'Вставить в ...', 'bwnuts' ),
		'uploaded_to_this_item' => __( 'Загружено для записи', 'bwnuts' ),
		'items_list'            => __( 'Список', 'bwnuts' ),
		'items_list_navigation' => __( 'Навигация', 'bwnuts' ),
		'filter_items_list'     => __( 'Фильтр', 'bwnuts' ),
	);
	$args = array(
		'label'                 => __( 'Упаковка', 'bwnuts' ),
		'description'           => __( 'Упаковка items', 'bwnuts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'revisions'  ),
		'taxonomies'            => array( 'pack_cat' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'pack', $args );

 
/**
 * Register a genre taxonomy.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
 */

/*$labels = array(
	'name'              => __( 'Genres', 'unyson' ),
	'singular_name'     => __( 'Genre', 'unyson' ),
	'search_items'      => __( 'Search Genres', 'unyson' ),
	'all_items'         => __( 'All Genres', 'unyson' ),
	'parent_item'       => __( 'Parent Genre', 'unyson' ),
	'parent_item_colon' => __( 'Parent Genre', 'unyson' ) . ':',
	'edit_item'         => __( 'Edit Genre', 'unyson' ),
	'update_item'       => __( 'Update Genre', 'unyson' ),
	'add_new_item'      => __( 'Add New Genre', 'unyson' ),
	'new_item_name'     => __( 'New Genre Name', 'unyson' ),
	'menu_name'         => __( 'Genre', 'unyson' ),
);

$args = array(
	'hierarchical'      => true,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	'rewrite'           => array( 'slug' => 'genre' ),
);

register_taxonomy( 'genre', array( 'book' ), $args );*/

$labels = array(
	'name'              => __( 'Категории упаковки', 'unyson' ),
	'singular_name'     => __( 'Категория упаковки', 'unyson' ),
	'search_items'      => __( 'Поиск в категорияз', 'unyson' ),
	'all_items'         => __( 'Все упаковки', 'unyson' ),
	'parent_item'       => __( 'Родительская категория', 'unyson' ),
	'parent_item_colon' => __( 'Родительская категория', 'unyson' ) . ':',
	'edit_item'         => __( 'Редактировать ', 'unyson' ),
	'update_item'       => __( 'Изменить', 'unyson' ),
	'add_new_item'      => __( 'Добавить новую категорию', 'unyson' ),
	'new_item_name'     => __( 'Новая категория упаковки', 'unyson' ),
	'menu_name'         => __( 'Категории упаковки', 'unyson' ),
);

$args = array(
	'hierarchical'      => true,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	'rewrite'           => array( 'slug' => 'pack_cat' )
);

register_taxonomy( 'pack_cat', array( 'pack' ), $args );