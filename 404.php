<?php
/**
 * The Template for displaying all single posts
 */

get_header();
?>

<?php 
if(pll_current_language() != 'ru') {
	$rootUrl = get_site_url().'/'.pll_current_language(); 
}else {
	$rootUrl = get_site_url();
}
?>

<div class="l-container">
    <div class="c-404">
    	<h3 class="c-404__header"><?php _e( 'Not Found', 'unyson' ); ?></h3>
      	<a href="<?php echo $rootUrl; ?>" class="c-404__link"><?php _e( 'Home page', 'unyson' ); ?></a>
    </div>
</div>      
<?php
get_footer();
