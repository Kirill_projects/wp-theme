<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 */

get_header();
function get_words($sentence, $count = 18) {
  return implode(' ', array_slice(explode(' ', $sentence), 0, $count))."&hellip;";
}
global $post;
$category = get_the_category()[0];
?>


<section class="container posts">
	<h2 class="big-header__green text-uppercase big-header__center">
            
                <img src="<?php echo get_template_directory_uri(); ?>/img/header-line-left.png" alt="">
                <?php  echo $category->name; ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/header-line-right.png" alt="">
            </h2>
	<div class="row posts-items">
		<?php 
					$args = array( 'posts_per_page' => 24,  'category' => $category->cat_ID );
					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
			<div class="col-lg-3 col-md-6 post-item">
				<img src="<?php the_post_thumbnail_url('news-thumb')?>" alt="" class='post-item__image'>
				<div class='post-item__header text-uppercase'><?php the_title(); ?></div>
				<div class='post-item__text'>
					<?php echo  get_words(get_the_excerpt()); ?>
				</div>
			</div>
		</a>
		<?php endforeach; wp_reset_postdata();  ?>
	</div>
</section> <?php get_footer(); ?>